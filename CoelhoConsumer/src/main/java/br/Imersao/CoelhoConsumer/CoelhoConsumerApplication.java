package br.Imersao.CoelhoConsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CoelhoConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CoelhoConsumerApplication.class, args);
	}

}
