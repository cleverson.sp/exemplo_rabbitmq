package br.Imersao.CoelhoConsumer.services;

import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@RabbitListener(queues = "#{temporaria.name}")
public class CoelhoService {
	
	@RabbitHandler
	public void comerCenoura(String descricao) {
		System.out.println("Cenoura "+descricao+" devorada pelo consumer 2 ;)");
	}
}
