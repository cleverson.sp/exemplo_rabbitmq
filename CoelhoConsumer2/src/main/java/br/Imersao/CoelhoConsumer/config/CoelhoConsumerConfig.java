package br.Imersao.CoelhoConsumer.config;

import org.springframework.amqp.core.AnonymousQueue;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.FanoutExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.amqp.core.Binding;

@Configuration
public class CoelhoConsumerConfig {
	
	@Bean
	public FanoutExchange fanout() {
		return new FanoutExchange("pubSubber");
	}
	
	@Bean
	public Queue temporaria() {
		return new AnonymousQueue();
	}
	
	@Bean
	public Binding bindeiro (FanoutExchange fanout, Queue temporaria) {
		
		return BindingBuilder.bind(temporaria).to(fanout);
		
	}

}
