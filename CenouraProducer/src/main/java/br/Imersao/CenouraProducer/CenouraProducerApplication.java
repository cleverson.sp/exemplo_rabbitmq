package br.Imersao.CenouraProducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CenouraProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(CenouraProducerApplication.class, args);
	}

}
