package br.Imersao.CenouraProducer.config;

import org.springframework.amqp.core.FanoutExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CenouraProducerConfig {

	@Bean
	public FanoutExchange fanout() {
		return new FanoutExchange("pubSubber");
	}

//	@Bean
//	public Queue cenouras() {
//		return new Queue("cenouras");
//	}
//

}
